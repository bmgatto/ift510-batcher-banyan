#!/usr/bin/python3
# A Program to do the batcher and the banyan and the zip zop zoobity bop
# Shoutout to simpleflips

import random
import math
from operator import itemgetter

def batcherSort(arrayToSort):
    '''
       0   1   2   3   4   5
    a [▼] [▼] [▼] [▼] [▼] [▼]
    b [▲] [▼] [▼] [▼] [▼] [▼]
    c [▲] [▲] [▲] [▼] [▼] [▼]
    d [▼] [▲] [▲] [▼] [▼] [▼]
    
    batcherNet[a][1]
    '''
    
    # Here's the part that does the batcher sorting

    batcherNet = []
    for i in range(0,4):
        batcherNet.append([])
        for j in range(0,6):
            batcherNet[i].append([9, 9])

    yoink = sorted(arrayToSort, key=itemgetter(0))

    piglet = 0
    print() 
    for i in [1]:
        if yoink[piglet][0] == 1:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")

        if piglet >= numOfPackets:
            print("             ----|\n" * 7)
            break

        if yoink[piglet][0] == 2:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")

        if piglet >= numOfPackets:
            print("             ----|\n" * 6)
            break

        if yoink[piglet][0] == 3:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")

        if piglet >= numOfPackets:
            print("             ----|\n" * 5)
            break

        if yoink[piglet][0] == 4:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")
        
        if piglet >= numOfPackets:
            print("             ----|\n" * 4)
            break
        
        if yoink[piglet][0] == 5:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")
        
        if piglet >= numOfPackets:
            print("             ----|\n" * 3)
            break
      
        if yoink[piglet][0] == 6:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")
        
        if piglet >= numOfPackets:
            print("             ----|\n" * 2)
            break
        
        if yoink[piglet][0] == 7:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")
        
        if piglet >= numOfPackets:
            print("             ----|\n" * 1)
            break
        
        if yoink[piglet][0] == 8:
            print(yoink[piglet], "----|")
            piglet += 1
        else:
            print("             ----|")


    print()
    print("Batcher network")
    for i in range(0, numOfPackets):
        woahnum = max(0, math.ceil(arrayToSort[i][0] / 2) - 1)
        if batcherNet[woahnum][0][0] == 9:
            batcherNet[woahnum][0][0] = arrayToSort[i][0]
        else:
            batcherNet[woahnum][0][1] = arrayToSort[i][0]

    for i in batcherNet:
        print(i)

    if batcherNet[0][0][0] < batcherNet[0][0][1]:
        if batcherNet[0][1][0] == 9:
            batcherNet[0][1][0] = batcherNet[0][0][0]
        else:
            batcherNet[0][1][1] = batcherNet[0][0][0]
        if batcherNet[1][1][0] == 9:
            batcherNet[1][1][0] = batcherNet[0][0][1]
        else:
            batcherNet[1][1][1] = batcherNet[0][0][1]
    else:
        if batcherNet[0][1][0] == 9:
            batcherNet[0][1][0] = batcherNet[0][0][1]
        else:
            batcherNet[0][1][1] = batcherNet[0][0][1]
        if batcherNet[1][1][0] == 9:
            batcherNet[1][1][0] = batcherNet[0][0][0]
        else:
            batcherNet[1][1][1] = batcherNet[0][0][0]

    if batcherNet[1][0][0] > batcherNet[1][0][1]:
        if batcherNet[0][1][0] == 9:
            batcherNet[0][1][0] = batcherNet[1][0][0]
        else:
            batcherNet[0][1][1] = batcherNet[1][0][0]
        if batcherNet[1][1][0] == 9:
            batcherNet[1][1][0] = batcherNet[1][0][1]
        else:
            batcherNet[1][1][1] = batcherNet[1][0][1]
    else:
        if batcherNet[0][1][0] == 9:
            batcherNet[0][1][0] = batcherNet[1][0][1]
        else:
            batcherNet[0][1][1] = batcherNet[1][0][1]
        if batcherNet[1][1][0] == 9:
            batcherNet[1][1][0] = batcherNet[1][0][0]
        else:
            batcherNet[1][1][1] = batcherNet[1][0][0]

    if batcherNet[2][0][0] > batcherNet[2][0][1]:
        if batcherNet[2][1][0] == 9:
            batcherNet[2][1][0] = batcherNet[2][0][0]
        else:
            batcherNet[2][1][1] = batcherNet[2][0][0]
        if batcherNet[3][1][0] == 9:
            batcherNet[3][1][0] = batcherNet[2][0][1]
        else:
            batcherNet[3][1][1] = batcherNet[2][0][1]
    else:
        if batcherNet[2][1][0] == 9:
            batcherNet[2][1][0] = batcherNet[2][0][1]
        else:
            batcherNet[2][1][1] = batcherNet[2][0][1]
        if batcherNet[3][1][0] == 9:
            batcherNet[3][1][0] = batcherNet[2][0][0]
        else:
            batcherNet[3][1][1] = batcherNet[2][0][0]

    if batcherNet[3][0][0] < batcherNet[3][0][1]:
        if batcherNet[2][1][0] == 9:
            batcherNet[2][1][0] = batcherNet[3][0][0]
        else:
            batcherNet[2][1][1] = batcherNet[3][0][0]
        if batcherNet[3][1][0] == 9:
            batcherNet[3][1][0] = batcherNet[3][0][1]
        else:
            batcherNet[3][1][1] = batcherNet[3][0][1]
    else:
        if batcherNet[2][1][0] == 9:
            batcherNet[2][1][0] = batcherNet[3][0][1]
        else:
            batcherNet[2][1][1] = batcherNet[3][0][1]
        if batcherNet[3][1][0] == 9:
            batcherNet[3][1][0] = batcherNet[3][0][0]
        else:
            batcherNet[3][1][1] = batcherNet[3][0][0]

    print()
    for i in batcherNet:
        print(i)

    if batcherNet[0][1][0] < batcherNet[0][1][1]:
        if batcherNet[0][2][0] == 9:
            batcherNet[0][2][0] = batcherNet[0][1][0]
        else:
            batcherNet[0][2][1] = batcherNet[0][1][0]
        if batcherNet[1][2][0] == 9:
            batcherNet[1][2][0] = batcherNet[0][1][1]
        else:
            batcherNet[1][2][1] = batcherNet[0][1][1]
    else:
        if batcherNet[0][2][0] == 9:
            batcherNet[0][2][0] = batcherNet[0][1][1]
        else:
            batcherNet[0][2][1] = batcherNet[0][1][1]
        if batcherNet[1][2][0] == 9:
            batcherNet[1][2][0] = batcherNet[0][1][0]
        else:
            batcherNet[1][2][1] = batcherNet[0][1][0]

    if batcherNet[1][1][0] < batcherNet[1][1][1]:
        if batcherNet[0][2][0] == 9:
            batcherNet[0][2][0] = batcherNet[1][1][0]
        else:
            batcherNet[0][2][1] = batcherNet[1][1][0]
        if batcherNet[1][2][0] == 9:
            batcherNet[1][2][0] = batcherNet[1][1][1]
        else:
            batcherNet[1][2][1] = batcherNet[1][1][1]
    else:
        if batcherNet[0][2][0] == 9:
            batcherNet[0][2][0] = batcherNet[1][1][1]
        else:
            batcherNet[0][2][1] = batcherNet[1][1][1]
        if batcherNet[1][2][0] == 9:
            batcherNet[1][2][0] = batcherNet[1][1][0]
        else:
            batcherNet[1][2][1] = batcherNet[1][1][0]

    if batcherNet[2][1][0] > batcherNet[2][1][1]:
        if batcherNet[2][2][0] == 9:
            batcherNet[2][2][0] = batcherNet[2][1][0]
        else:
            batcherNet[2][2][1] = batcherNet[2][1][0]
        if batcherNet[3][2][0] == 9:
            batcherNet[3][2][0] = batcherNet[2][1][1]
        else:
            batcherNet[3][2][1] = batcherNet[2][1][1]
    else:
        if batcherNet[2][2][0] == 9:
            batcherNet[2][2][0] = batcherNet[2][1][1]
        else:
            batcherNet[2][2][1] = batcherNet[2][1][1]
        if batcherNet[3][2][0] == 9:
            batcherNet[3][2][0] = batcherNet[2][1][0]
        else:
            batcherNet[3][2][1] = batcherNet[2][1][0]

    if batcherNet[3][1][0] > batcherNet[3][1][1]:
        if batcherNet[2][2][0] == 9:
            batcherNet[2][2][0] = batcherNet[3][1][0]
        else:
            batcherNet[2][2][1] = batcherNet[3][1][0]
        if batcherNet[3][2][0] == 9:
            batcherNet[3][2][0] = batcherNet[3][1][1]
        else:
            batcherNet[3][2][1] = batcherNet[3][1][1]
    else:
        if batcherNet[2][2][0] == 9:
            batcherNet[2][2][0] = batcherNet[3][1][1]
        else:
            batcherNet[2][2][1] = batcherNet[3][1][1]
        if batcherNet[3][2][0] == 9:
            batcherNet[3][2][0] = batcherNet[3][1][0]
        else:
            batcherNet[3][2][1] = batcherNet[3][1][0]

    print()
    for i in batcherNet:
        print(i)

    if batcherNet[0][2][0] < batcherNet[0][2][1]:
        if batcherNet[0][3][0] == 9:
            batcherNet[0][3][0] = batcherNet[0][2][0]
        else:
            batcherNet[0][3][1] = batcherNet[0][2][0]
        if batcherNet[1][3][0] == 9:
            batcherNet[1][3][0] = batcherNet[0][2][1]
        else:
            batcherNet[1][3][1] = batcherNet[0][2][1]
    else:
        if batcherNet[0][3][0] == 9:
            batcherNet[0][3][0] = batcherNet[0][2][1]
        else:
            batcherNet[0][3][1] = batcherNet[0][2][1]
        if batcherNet[1][3][0] == 9:
            batcherNet[1][3][0] = batcherNet[0][2][0]
        else:
            batcherNet[1][3][1] = batcherNet[0][2][0]

    if batcherNet[1][2][0] < batcherNet[1][2][1]:
        if batcherNet[2][3][0] == 9:
            batcherNet[2][3][0] = batcherNet[1][2][0]
        else:
            batcherNet[2][3][1] = batcherNet[1][2][0]
        if batcherNet[3][3][0] == 9:
            batcherNet[3][3][0] = batcherNet[1][2][1]
        else:
            batcherNet[3][3][1] = batcherNet[1][2][1]
    else:
        if batcherNet[2][3][0] == 9:
            batcherNet[2][3][0] = batcherNet[1][2][1]
        else:
            batcherNet[2][3][1] = batcherNet[1][2][1]
        if batcherNet[3][3][0] == 9:
            batcherNet[3][3][0] = batcherNet[1][2][0]
        else:
            batcherNet[3][3][1] = batcherNet[1][2][0]

    if batcherNet[2][2][0] > batcherNet[2][2][1]:
        if batcherNet[0][3][0] == 9:
            batcherNet[0][3][0] = batcherNet[2][2][0]
        else:
            batcherNet[0][3][1] = batcherNet[2][2][0]
        if batcherNet[1][3][0] == 9:
            batcherNet[1][3][0] = batcherNet[2][2][1]
        else:
            batcherNet[1][3][1] = batcherNet[2][2][1]
    else:
        if batcherNet[0][3][0] == 9:
            batcherNet[0][3][0] = batcherNet[2][2][1]
        else:
            batcherNet[0][3][1] = batcherNet[2][2][1]
        if batcherNet[1][3][0] == 9:
            batcherNet[1][3][0] = batcherNet[2][2][0]
        else:
            batcherNet[1][3][1] = batcherNet[2][2][0]

    if batcherNet[3][2][0] > batcherNet[3][2][1]:
        if batcherNet[2][3][0] == 9:
            batcherNet[2][3][0] = batcherNet[3][2][0]
        else:
            batcherNet[2][3][1] = batcherNet[3][2][0]
        if batcherNet[3][3][0] == 9:
            batcherNet[3][3][0] = batcherNet[3][2][1]
        else:
            batcherNet[3][3][1] = batcherNet[3][2][1]
    else:
        if batcherNet[2][3][0] == 9:
            batcherNet[2][3][0] = batcherNet[3][2][1]
        else:
            batcherNet[2][3][1] = batcherNet[3][2][1]
        if batcherNet[3][3][0] == 9:
            batcherNet[3][3][0] = batcherNet[3][2][0]
        else:
            batcherNet[3][3][1] = batcherNet[3][2][0]

    print()
    for i in batcherNet:
        print(i)

    if batcherNet[0][3][0] < batcherNet[0][3][1]:
        if batcherNet[0][4][0] == 9:
            batcherNet[0][4][0] = batcherNet[0][3][0]
        else:
            batcherNet[0][4][1] = batcherNet[0][3][0]
        if batcherNet[1][4][0] == 9:
            batcherNet[1][4][0] = batcherNet[0][3][1]
        else:
            batcherNet[1][4][1] = batcherNet[0][3][1]
    else:
        if batcherNet[0][4][0] == 9:
            batcherNet[0][4][0] = batcherNet[0][3][1]
        else:
            batcherNet[0][4][1] = batcherNet[0][3][1]
        if batcherNet[1][4][0] == 9:
            batcherNet[1][4][0] = batcherNet[0][3][0]
        else:
            batcherNet[1][4][1] = batcherNet[0][3][0]

    if batcherNet[1][3][0] < batcherNet[1][3][1]:
        if batcherNet[2][4][0] == 9:
            batcherNet[2][4][0] = batcherNet[1][3][0]
        else:
            batcherNet[2][4][1] = batcherNet[1][3][0]
        if batcherNet[3][4][0] == 9:
            batcherNet[3][4][0] = batcherNet[1][3][1]
        else:
            batcherNet[3][4][1] = batcherNet[1][3][1]
    else:
        if batcherNet[2][4][0] == 9:
            batcherNet[2][4][0] = batcherNet[1][3][1]
        else:
            batcherNet[2][4][1] = batcherNet[1][3][1]
        if batcherNet[3][4][0] == 9:
            batcherNet[3][4][0] = batcherNet[1][3][0]
        else:
            batcherNet[3][4][1] = batcherNet[1][3][0]

    if batcherNet[2][3][0] < batcherNet[2][3][1]:
        if batcherNet[0][4][0] == 9:
            batcherNet[0][4][0] = batcherNet[2][3][0]
        else:
            batcherNet[0][4][1] = batcherNet[2][3][0]
        if batcherNet[1][4][0] == 9:
            batcherNet[1][4][0] = batcherNet[2][3][1]
        else:
            batcherNet[1][4][1] = batcherNet[2][3][1]
    else:
        if batcherNet[0][4][0] == 9:
            batcherNet[0][4][0] = batcherNet[2][3][1]
        else:
            batcherNet[0][4][1] = batcherNet[2][3][1]
        if batcherNet[1][4][0] == 9:
            batcherNet[1][4][0] = batcherNet[2][3][0]
        else:
            batcherNet[1][4][1] = batcherNet[2][3][0]

    if batcherNet[3][3][0] < batcherNet[3][3][1]:
        if batcherNet[2][4][0] == 9:
            batcherNet[2][4][0] = batcherNet[3][3][0]
        else:
            batcherNet[2][4][1] = batcherNet[3][3][0]
        if batcherNet[3][4][0] == 9:
            batcherNet[3][4][0] = batcherNet[3][3][1]
        else:
            batcherNet[3][4][1] = batcherNet[3][3][1]
    else:
        if batcherNet[2][4][0] == 9:
            batcherNet[2][4][0] = batcherNet[3][3][1]
        else:
            batcherNet[2][4][1] = batcherNet[3][3][1]
        if batcherNet[3][4][0] == 9:
            batcherNet[3][4][0] = batcherNet[3][3][0]
        else:
            batcherNet[3][4][1] = batcherNet[3][3][0]

    print()
    for i in batcherNet:
        print(i)

    if batcherNet[0][4][0] < batcherNet[0][4][1]:
        if batcherNet[0][5][0] == 9:
            batcherNet[0][5][0] = batcherNet[0][4][0]
        else:
            batcherNet[0][5][1] = batcherNet[0][4][0]
        if batcherNet[1][5][0] == 9:
            batcherNet[1][5][0] = batcherNet[0][4][1]
        else:
            batcherNet[1][5][1] = batcherNet[0][4][1]
    else:
        if batcherNet[0][5][0] == 9:
            batcherNet[0][5][0] = batcherNet[0][4][1]
        else:
            batcherNet[0][5][1] = batcherNet[0][4][1]
        if batcherNet[1][5][0] == 9:
            batcherNet[1][5][0] = batcherNet[0][4][0]
        else:
            batcherNet[1][5][1] = batcherNet[0][4][0]

    if batcherNet[1][4][0] < batcherNet[1][4][1]:
        if batcherNet[2][5][0] == 9:
            batcherNet[2][5][0] = batcherNet[1][4][0]
        else:
            batcherNet[2][5][1] = batcherNet[1][4][0]
        if batcherNet[3][5][0] == 9:
            batcherNet[3][5][0] = batcherNet[1][4][1]
        else:
            batcherNet[3][5][1] = batcherNet[1][4][1]
    else:
        if batcherNet[2][5][0] == 9:
            batcherNet[2][5][0] = batcherNet[1][4][1]
        else:
            batcherNet[2][5][1] = batcherNet[1][4][1]
        if batcherNet[3][5][0] == 9:
            batcherNet[3][5][0] = batcherNet[1][4][0]
        else:
            batcherNet[3][5][1] = batcherNet[1][4][0]

    if batcherNet[2][4][0] < batcherNet[2][4][1]:
        if batcherNet[0][5][0] == 9:
            batcherNet[0][5][0] = batcherNet[2][4][0]
        else:
            batcherNet[0][5][1] = batcherNet[2][4][0]
        if batcherNet[1][5][0] == 9:
            batcherNet[1][5][0] = batcherNet[2][4][1]
        else:
            batcherNet[1][5][1] = batcherNet[2][4][1]
    else:
        if batcherNet[0][5][0] == 9:
            batcherNet[0][5][0] = batcherNet[2][4][1]
        else:
            batcherNet[0][5][1] = batcherNet[2][4][1]
        if batcherNet[1][5][0] == 9:
            batcherNet[1][5][0] = batcherNet[2][4][0]
        else:
            batcherNet[1][5][1] = batcherNet[2][4][0]

    if batcherNet[3][4][0] < batcherNet[3][4][1]:
        if batcherNet[2][5][0] == 9:
            batcherNet[2][5][0] = batcherNet[3][4][0]
        else:
            batcherNet[2][5][1] = batcherNet[3][4][0]
        if batcherNet[3][5][0] == 9:
            batcherNet[3][5][0] = batcherNet[3][4][1]
        else:
            batcherNet[3][5][1] = batcherNet[3][4][1]
    else:
        if batcherNet[2][5][0] == 9:
            batcherNet[2][5][0] = batcherNet[3][4][1]
        else:
            batcherNet[2][5][1] = batcherNet[3][4][1]
        if batcherNet[3][5][0] == 9:
            batcherNet[3][5][0] = batcherNet[3][4][0]
        else:
            batcherNet[3][5][1] = batcherNet[3][4][0]

    print()
    for i in batcherNet:
        print(i)

    if batcherNet[0][5][0] < batcherNet[0][5][1]:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[0][5][0]:
                arrayToSort[i].append(1)
            if arrayToSort[i][0] == batcherNet[0][5][1]:
                arrayToSort[i].append(2)
    else:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[0][5][1]:
                arrayToSort[i].append(1)
            if arrayToSort[i][0] == batcherNet[0][5][0]:
                arrayToSort[i].append(2)

    if batcherNet[1][5][0] < batcherNet[1][5][1]:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[1][5][0]:
                arrayToSort[i].append(3)
            if arrayToSort[i][0] == batcherNet[1][5][1]:
                arrayToSort[i].append(4)
    else:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[1][5][1]:
                arrayToSort[i].append(3)
            if arrayToSort[i][0] == batcherNet[1][5][0]:
                arrayToSort[i].append(4)

    if batcherNet[2][5][0] < batcherNet[2][5][1]:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[2][5][0]:
                arrayToSort[i].append(5)
            if arrayToSort[i][0] == batcherNet[2][5][1]:
                arrayToSort[i].append(6)
    else:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[2][5][1]:
                arrayToSort[i].append(5)
            if arrayToSort[i][0] == batcherNet[2][5][0]:
                arrayToSort[i].append(6)

    if batcherNet[3][5][0] < batcherNet[3][5][1]:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[3][5][0]:
                arrayToSort[i].append(7)
            if arrayToSort[i][0] == batcherNet[3][5][1]:
                arrayToSort[i].append(8)
    else:
        for i in range(0, numOfPackets):
            if arrayToSort[i][0] == batcherNet[3][5][1]:
                arrayToSort[i].append(1)
            if arrayToSort[i][0] == batcherNet[3][5][0]:
                arrayToSort[i].append(2)

    sortedArray = sorted(arrayToSort, key=itemgetter(4))

    print()
    for i in sortedArray:
        i.pop(4)
        print(i)

    return sortedArray

def banyanSort(arrayToSort):
    '''
       0   1   2
    a [ ] [ ] [ ]
    b [ ] [ ] [ ]
    c [ ] [ ] [ ]
    d [ ] [ ] [ ]
    
    banyanNet[a][1]
    '''
    # Here's the part that does the Banyan sorting
    
    banyanNet = []
    for i in range(0,4):
        banyanNet.append([])
        for j in range(0,3):
            banyanNet[i].append(9)

#    x = 0
#    y = 0
#    for i in arrayToSort:
#        banyanNet[x][0][y] = i[0]
#        if x < 3:
#            x += 1
#        else:
#            x = 0
#            y += 1

    print("\nBanyan Network")
    for i in banyanNet:
        print(i)

    x = 0
    places = ["30", "20", "10", "00", "30", "20", "10", "00"]
    for i in arrayToSort:
        banyanNet = []
        for a in range(0,4):
            banyanNet.append([])
            for b in range(0,3):
                banyanNet[a].append(9)

        banyanNet[x][0] = i[0]
        if x < 3:
            x += 1
        else:
            x = 0

        place = places.pop()
        if place == "00":
            if i[1] == 0:
                banyanNet[0][1] = i[0]
                place = "01"
            elif i[1] == 1:
                banyanNet[2][1] = i[0]
                place = "21"

        if place == "10":
            if i[1] == 0:
                banyanNet[1][1] = i[0]
                place = "11"
            elif i[1] == 1:
                banyanNet[3][1] = i[0]
                place = "31"

        if place == "20":
            if i[1] == 0:
                banyanNet[0][1] = i[0]
                place = "01"
            elif i[1] == 1:
                banyanNet[2][1] = i[0]
                place = "21"

        if place == "30":
            if i[1] == 0:
                banyanNet[1][1] = i[0]
                place = "11"
            elif i[1] == 1:
                banyanNet[3][1] = i[0]
                place = "31"



        if place == "01":
            if i[2] == 0:
                banyanNet[0][2] = i[0]
                place = "02"
            elif i[2] == 1:
                banyanNet[1][2] = i[0]
                place = "12"

        if place == "11":
            if i[2] == 0:
                banyanNet[0][2] = i[0]
                place = "02"
            elif i[2] == 1:
                banyanNet[1][2] = i[0]
                place = "12"

        if place == "21":
            if i[2] == 0:
                banyanNet[2][2] = i[0]
                place = "22"
            elif i[2] == 1:
                banyanNet[3][2] = i[0]
                place = "32"

        if place == "31":
            if i[2] == 0:
                banyanNet[2][2] = i[0]
                place = "22"
            elif i[2] == 1:
                banyanNet[3][2] = i[0]
                place = "32"



        if place == "02":
            if i[3] == 0:
                banyanNet[0].append("Output port 1")
                i.append(1)
            elif i[3] == 1:
                banyanNet[0].append("Output port 2")
                i.append(2)

        if place == "12":
            if i[3] == 0:
                banyanNet[1].append("Output port 3")
                i.append(3)
            elif i[3] == 1:
                banyanNet[1].append("Output port 4")
                i.append(4)

        if place == "22":
            if i[3] == 0:
                banyanNet[2].append("Output port 5")
                i.append(5)
            elif i[3] == 1:
                banyanNet[2].append("Output port 6")
                i.append(6)

        if place == "32":
            if i[3] == 0:
                banyanNet[3].append("Output port 7")
                i.append(7)
            elif i[3] == 1:
                banyanNet[3].append("Output port 8")
                i.append(8)

        print()
        for i in banyanNet:
            print(i)
        
    sortedArray = sorted(arrayToSort, key=itemgetter(4))

    piglet = 0
    print() 
    for i in [1]:
        if sortedArray[piglet][4] == 1:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")

        if piglet >= numOfPackets:
            print("|----\n" * 7)
            break

        if sortedArray[piglet][4] == 2:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")

        if piglet >= numOfPackets:
            print("|----\n" * 6)
            break

        if sortedArray[piglet][4] == 3:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")

        if piglet >= numOfPackets:
            print("|----\n" * 5)
            break

        if sortedArray[piglet][4] == 4:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")
        
        if piglet >= numOfPackets:
            print("|----\n" * 4)
            break
        
        if sortedArray[piglet][4] == 5:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")
        
        if piglet >= numOfPackets:
            print("|----\n" * 3)
            break
      
        if sortedArray[piglet][4] == 6:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")
        
        if piglet >= numOfPackets:
            print("|----\n" * 2)
            break
        
        if sortedArray[piglet][4] == 7:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")
        
        if piglet >= numOfPackets:
            print("|----\n" * 1)
            break
        
        if sortedArray[piglet][4] == 8:
            sortedArray[piglet].pop(4)
            print("|----", sortedArray[piglet])
            piglet += 1
        else:
            print("|----")

    return(sortedArray)





def randomPackets(numOfPackets):
    # This generates the packets
    packetsList = []
    packetsPaths = []
    # Generate packet number
    for i in range(0,numOfPackets):
        addressNum.append([])
        rannum = random.randint(1,8)
        while rannum in packetsList:
            rannum = random.randint(1,8)
        packetsList.append(rannum)
        addressNum[i].append(rannum)
        
        ranpath = str(random.randint(0,1)) + str(random.randint(0,1)) + str(random.randint(0,1))
        while ranpath in packetsPaths:
            ranpath = str(random.randint(0,1)) + str(random.randint(0,1)) + str(random.randint(0,1))
        packetsPaths.append(ranpath)
        for j in list(ranpath):
            addressNum[i].append(int(j))




#############################################################



addressNum = []
numOfPackets = int(input("Num of Packets: "))

randomPackets(numOfPackets)
#for i in range(0, numOfPackets):
#    print(addressNum[i])
addressNum = batcherSort(addressNum)
addressNum = banyanSort(addressNum)
